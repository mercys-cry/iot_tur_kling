# File Structure
The repository contains multiple folders which intern contain code that must be installed in the correct place.
- The soundAnalysis folder contains the program to be flashed on the ESP-32.
- The OpenHAB file contains code relevant to the OpenHAB Rules.

# OpenHAB
Instructions on how to set-up the OpenHABian environment on a rasberry Pi can be found [here](https://www.hackster.io/455149/smarte-tonerkennung-3ba631). 
The [OpenHAB_Rule.js](https://gitlab.com/mercys-cry/iot_tur_kling/-/blob/main/OpenHAB/OpenHAB_Rule.js) contains the javascript code for the OpenHABian rule which is to be implemented in the OpenHABian environment.

The following changes must be made to the code for it to function:
- [OpenHAB_Rule.js line 5](https://gitlab.com/mercys-cry/iot_tur_kling/-/blob/main/OpenHAB/OpenHAB_Rule.js#L5) Replace X with Item_Name_OpenHAB-String
- [OpenHAB_Rule.js line 6](https://gitlab.com/mercys-cry/iot_tur_kling/-/blob/main/OpenHAB/OpenHAB_Rule.js#L6) Replace X with OpenHAB identifier for the THING - e.x. "dedf23c1b4"

All other required explanations can be found commented in the code.

# ESP-32
The entire [soundAnalyse folder](https://gitlab.com/mercys-cry/iot_tur_kling/-/tree/main/soundAnalyse) is to be installed on the ESP-32 using the Arduino IDE.

The following changes must be made to the code for it to function:
- [soundAnalyse.ino line 42](https://gitlab.com/mercys-cry/iot_tur_kling/-/blob/main/soundAnalyse/soundAnalyse.ino#L42) Change IP to your own MQTT Broker IP
- [soundAnalyse.ino line 46](https://gitlab.com/mercys-cry/iot_tur_kling/-/blob/main/soundAnalyse/soundAnalyse.ino#L46) Change to desired topic ID

All other required explanations can be found commented in the code.
