 /********************************************************************************************************************************************************
 *                                                                                                                                                       *
 *  Project:         Smarte Ton Erkennung									                                                                       *
 *	Name:		V.v.Herz, C.v.Caron, N.K, L.E																							  *
 *  Target Platform: ESP32                                                                                                                               *
 *                                                                                                                                                       *
 *                                                                                                                                                       *
 *  Mark Donners                                                                                                                                         *
 *  The Electronic Engineer                                                                                                                              *
 *  github:    https://github.com/donnersm                                                                                                               *
 *                                                                                                                                                       *
 ********************************************************************************************************************************************************/
/*Required Changes:
line 42
line 46
*/

//general libaries
#include <arduinoFFT.h>                                 //libary for FFT analysis

//included files
#include "I2SPLUGIN.h"                                  //Setting up the ADC for I2S interface ( very fast readout)
#include "FFT.h"                                        //some things for selecting the correct arrays for each number of bands
#include "Settings.h"                                   //your general settings
#include "wifiAndOTA.h"

//libaries for webinterface
#include <WiFi.h>
#include <PubSubClient.h>                          

const int numBands = 64;  //Default number of bands. change it by pressing the mode button
int timer = 0;
int intervall = 80;
int threshold = 180000;

int peakFreq;
int freqBand[numBands];
float allBandsPeak = 0;

//############ Step 0: MQTT Settings ##################
String msgString = "";
const char* mqtt_server = "192.168.0.11";	//Change IP to your MQTT Broker
int mqttPort = 1883;
const int msgLength = 50;
char msg[msgLength] = {'\0'};
char topicPub[] = "esp32/peakFreq";			//Change to desired topic ID

WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

void setup() {
    
  Serial.begin(115200);
  Serial.println("Setting up Audio Input I2S");
  setupI2S();
  delay(100);
  i2s_adc_enable(I2S_NUM_0);
  Serial.println("Audio input setup completed");   

  Serial.println("Setting up WifiManager and OTA");
  wifiManager();
  ota();
  
  SetNumberofBands(numBands);
  mqttClient.setServer(mqtt_server, mqttPort);
  mqttClient.setCallback(callback);
}

void loop() {
  ArduinoOTA.handle();
  if (!mqttClient.connected())
      reconnect();
  mqttClient.loop();
  
  size_t bytesRead = 0;
  
  //############ Step 1: read samples from the I2S Buffer ##################
  i2s_read(I2S_PORT,
           (void*)samples,
           sizeof(samples),
           &bytesRead,   // workaround This is the actual buffer size last half will be empty but why?
           portMAX_DELAY); // no timeout

  if (bytesRead != sizeof(samples)) {
    Serial.printf("Could only read %u bytes of %u in FillBufferI2S()\n", bytesRead, sizeof(samples));
  }

  //############ Step 2: compensate for Channel number and offset, safe all to vReal Array   ############
  for (uint16_t i = 0; i < ARRAYSIZE(samples); i++) {
    vReal[i] = offset - samples[i];
    vImag[i] = 0.0; //Imaginary part must be zeroed in case of looping to avoid wrong calculations and overflows
  }

  //############ Step 3: Do FFT on the VReal array  ############
  // compute FFT
  FFT.DCRemoval();
  FFT.Windowing(vReal, SAMPLEBLOCK, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
  FFT.Compute(vReal, vImag, SAMPLEBLOCK, FFT_FORWARD);
  FFT.ComplexToMagnitude(vReal, vImag, SAMPLEBLOCK);
  FFT.MajorPeak(vReal, SAMPLEBLOCK, samplingFrequency);
  for (int i = 0; i < numBands; i++) {
    FreqBins[i] = 0;
  }
  
  //############ Step 4: Fill the frequency bins with the FFT Samples ############
  for (int i = 2; i < SAMPLEBLOCK / 2; i++) {
    if (vReal[i] > NoiseTresshold) {
      int freq = BucketFrequency(i);
      int iBand = 0;
      while (iBand < numBands) {
        if (freq < BandCutoffTable[iBand])break;
        iBand++;
      }
      if (iBand > numBands)iBand = numBands;
      FreqBins[iBand] += vReal[i];
    }
  }

  //############ Step 5: Finding the peak frequency
  allBandsPeak = 0;
  for (int i = 0; i < numBands; i++) {
    if (FreqBins[i] > allBandsPeak) {
      allBandsPeak = FreqBins[i];
      peakFreq = i;
    }
  }

  //############ Step 6: Saving all peak frequencies and sending the MQTT message
  if(allBandsPeak >= threshold || timer > 0){
    if(timer == 0){
      String allBandsPeakString = "Peak: " + String(allBandsPeak);
      Serial.println(allBandsPeakString);
    }
    timer++;

    if(timer < intervall){
      freqBand[peakFreq]++; 
    }else if(timer >= intervall){
      for (int i = 0; i < numBands; i++) {
        if(freqBand[i] > 5){
          if(msgString == ""){
            msgString = String(i) + "," + String(freqBand[i]);
          }else{
            msgString = msgString + ","  + String(i) + "," + String(freqBand[i]);
          }
        }
      }
      Serial.println(msgString);
      if(msgString.length()+1 <= msgLength){
        msgString.toCharArray(msg, msgString.length() + 1);
        mqttClient.publish(topicPub, msg);   
      }
      resetAll();
    } 
  } 
} // End loop()

void resetAll(){
  msgString = "";
  timer = 0;
  for (int i = 0; i < numBands; i++) {
    freqBand[i] = 0;
  }
}

// Return the frequency corresponding to the Nth sample bucket.  Skips the first two
// buckets which are overall amplitude and something else.
int BucketFrequency(int iBucket) {
  if (iBucket <= 1)return 0;
  int iOffset = iBucket - 2;
  return iOffset * (samplingFrequency / 2) / (SAMPLEBLOCK / 2);
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Callback - ");
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
}

void reconnect() {
  Serial.println("Connecting to MQTT Broker...");
  while (!mqttClient.connected()) {
      Serial.println("Reconnecting to MQTT Broker...");
      String clientId = "ESP32Client";
      clientId += String(random(0xffff), HEX);
      
      if (mqttClient.connect(clientId.c_str())) {
        Serial.println("Connected to MQTT Broker");
      }
  }
}
