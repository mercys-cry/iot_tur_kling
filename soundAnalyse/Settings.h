 /********************************************************************************************************************************************************
 *                                                                                                                                                       *
 *  Project:         Smarte Ton Erkennung									                                                                       *
 *	Name:		V.v.Herz, C.v.Caron, N.K, L.E																							  *
 *  Target Platform: ESP32                                                                                                                               *
 *                                                                                                                                                       *
 *                                                                                                                                                       *
 *  Mark Donners                                                                                                                                         *
 *  The Electronic Engineer                                                                                                                              *
 *  github:    https://github.com/donnersm                                                                                                               *
 *                                                                                                                                                       *
 ********************************************************************************************************************************************************/
 
#pragma once

#define GAIN_DAMPEN         2                       // Higher values cause auto gain to react more slowly
int NoiseTresshold =        1000;                   // this will effect the upper bands most.

// Other stuff don't change
#define ARRAYSIZE(a)    (sizeof(a)/sizeof(a[0]))
#define ADC_INPUT ADC1_CHANNEL_0
uint16_t offset = (int)ADC_INPUT * 0x1000 + 0xFFF;
double vReal[SAMPLEBLOCK];
double vImag[SAMPLEBLOCK];
int16_t samples[SAMPLEBLOCK];
arduinoFFT FFT = arduinoFFT(); /* Create FFT object */
byte peak[65];
int oldBarHeights[65];
volatile float FreqBins[65];
volatile float FreqBinsOD[65];
volatile float FreqBinsNW[65];
