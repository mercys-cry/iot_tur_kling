//Replace X's - Or the Program won't work

var timeOut = 5000;

var msg = itemRegistry.getItem("X").getState().toString().split(',');	//Replace X with Item_Name_OpenHAB-String
var telegramAction = actions.get("telegram","telegram:telegramBot:X");	//Replace X with OpenHAB identifier for the THING - e.x. "dedf23c1b4"

for(var i = 0; i < msg.length; i++){
  if(i%2 == 0 && msg[i] == 20 && msg[i+1] > 60){		//Frequency0 at Frequency array position 20, Threashold should be triggered at least 60 times
    telegramAction.sendTelegram("Es hat geklingelt");				//Text seen on Telegram - for said Frequency
    java.lang.Thread.sleep(timeOut);
  }
  else if(i%2 == 0 && msg[i] == 48 && msg[i+1] > 20){	//Frequency1 at Frequency array position 48, Threashold should be triggered at least 20 times
    for(var j = i; j < msg.length; j++){
      if(j%2 == 0 && msg[j] == 63 && msg[j+1] > 20){	//Frequency2 at Frequency array position 63, Threashold should be triggered at least 20 times
          telegramAction.sendTelegram("Waschmaschine ist fertig");	//Text seen on Telegram - for said Frequency
          java.lang.Thread.sleep(timeOut);
      }
    }
  }
  
  /* Frequency if statment - To check more frequencies, and another for and if loop
  @Text here@ - insert your own code
  64 possible frequencies - from 0 to 63
  
    else if(i%2 == 0 && msg[i] == @Frequency_Array-Interger@ && msg[i+1] > @Total_Threshold_Triggers-Interger@){
		for(var j = i; j < msg.length; j++){
		  if(j%2 == 0 && msg[j] == @Frequency_Array-Interger@ && msg[j+1] > @Total_Threshold_Triggers-Interger@){
			  telegramAction.sendTelegram("@Sound_Text-String@");
			  java.lang.Thread.sleep(timeOut);
		  }
		}
	}
	
  */
}